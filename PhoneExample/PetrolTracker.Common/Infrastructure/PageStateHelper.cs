﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.Generic;

namespace PetrolTracker.Common.Infrastructure 
{
    /// <summary>
    /// Represents a PageStateHelper that is used to retrieve and cast objects saved in page state.
    /// </summary>
    public class PageStateHelper 
    {    
        readonly IDictionary<string, object> state;

        /// <summary>
        /// Initializes a new instance of the <see cref="PageStateHelper"/> class.
        /// </summary>
        /// <param name="state">The page state dictionary.</param>
        public PageStateHelper(IDictionary<string, object> state)
        {
            if (state == null) throw new ArgumentNullException("state");
            this.state = state;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>Saved instance of T or the defaultValue</returns>
        public T GetValue<T>(string key, T defaultValue) 
        {
            object value;
            if (state.TryGetValue(key, out value)) 
            {
                if (value is T) 
                {
                    return (T) value;
                }
            }
            return defaultValue;
        }
    }
}