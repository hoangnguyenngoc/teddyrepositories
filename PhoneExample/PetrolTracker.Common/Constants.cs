﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;

namespace PetrolTracker.Common
{
    public class Constants
    {
        public const string FleetKey = "Fleet";
        public const string ChartIndexKey = "ChartIndexKey";
        public const string MpgChartStateName = "MpgState";
        public const string CostChartStateName = "CostState";
    }
}