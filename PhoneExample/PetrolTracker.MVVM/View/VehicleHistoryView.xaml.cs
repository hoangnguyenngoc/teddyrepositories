﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Windows.Navigation;
using PetrolTracker.Mvvm.ViewModel;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PetrolTracker.Mvvm.View
{
    public partial class VehicleHistoryView : PhoneApplicationPage
    {
        VehicleHistoryViewModel vehicleHistoryViewModel;

        VehicleHistoryViewModel VehicleHistoryViewModel
        {
            get { return vehicleHistoryViewModel ?? (vehicleHistoryViewModel = this.DataContext as VehicleHistoryViewModel); }
        }

        public VehicleHistoryView()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (this.VehicleHistoryViewModel != null)
            {
                bool isBeingActivated = PhoneApplicationService.Current.StartupMode == StartupMode.Activate;
                this.VehicleHistoryViewModel.NavigatedTo(isBeingActivated, this.State);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            // if not navigating back, save page state.
            if (e.NavigationMode == NavigationMode.Back) return;

            if (this.VehicleHistoryViewModel != null)
            {
                this.VehicleHistoryViewModel.NavigatedFrom(this.State);
            }
        }
    }
}