﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.ComponentModel;
using System.Windows.Navigation;
using PetrolTracker.Mvvm.ViewModel;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PetrolTracker.Mvvm.View 
{
    public partial class AddFillUpView : PhoneApplicationPage 
    {

        AddFillUpViewModel addFillUpViewModel;

        AddFillUpViewModel AddFillUpViewModel 
        {
            get { return addFillUpViewModel ?? (addFillUpViewModel = this.DataContext as AddFillUpViewModel); }
        }

        public AddFillUpView()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) 
        {
            base.OnNavigatedTo(e);

            if(this.AddFillUpViewModel != null) 
            {
                bool isBeingActivated = PhoneApplicationService.Current.StartupMode == StartupMode.Activate;
                this.AddFillUpViewModel.NavigatedTo(isBeingActivated, this.State);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) 
        {
            base.OnNavigatedFrom(e);

            // if not navigating back, save page state.
            if (e.NavigationMode == NavigationMode.Back) return;
            
            if(this.AddFillUpViewModel != null) 
            {
                this.AddFillUpViewModel.NavigatedFrom(this.State);
            }
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            if(this.AddFillUpViewModel != null)
            {
                e.Cancel = this.AddFillUpViewModel.CancelBackKeyNavigation();
            }
        }
    }
}