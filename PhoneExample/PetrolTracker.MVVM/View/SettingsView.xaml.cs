﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System.Windows.Navigation;
using PetrolTracker.Mvvm.ViewModel;
using Microsoft.Phone.Controls;

namespace PetrolTracker.Mvvm.View 
{
    public partial class SettingsView : PhoneApplicationPage 
    {

        SettingsViewModel settingsViewModel;

        SettingsViewModel SettingsViewModel
        {
            get { return settingsViewModel ?? (settingsViewModel = this.DataContext as SettingsViewModel); }
        }

        public SettingsView()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) 
        {
            base.OnNavigatedTo(e);

            if(this.SettingsViewModel != null) 
            {
                this.SettingsViewModel.NavigatedTo();
            }
        }
    }
}