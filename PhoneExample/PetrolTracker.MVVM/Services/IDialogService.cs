﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;

namespace PetrolTracker.Mvvm.Services
{
    public interface IDialogService
    {
        /// <summary>
        /// Displays a message box that contains the specified text, title bar caption, and response buttons.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <returns>Returns DialogResult.OK.</returns>
        DialogResult Show(string message);

        /// <summary>
        /// Displays a message box that contains the specified text, title bar caption, and response buttons.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="caption">The title of the message box.</param>
        /// <param name="button"> A value that indicates the button or buttons to display.</param>
        /// <returns>A value that indicates the user's response to the message.</returns>
        DialogResult Show(string message, string caption, DialogButton button);
    }
}