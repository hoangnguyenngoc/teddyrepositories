﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Windows;

namespace PetrolTracker.Mvvm.Services
{
    /// <summary>
    /// Very simple service abstraction over MessageBox that displays a message to the user and optionally prompts for a response.
    /// </summary>
    public class DialogService : IDialogService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:PetrolTracker.Mvvm.Services.DialogService"/> class.
        /// </summary>
        public DialogService()
        {
        }

        /// <summary>
        /// Displays a message box that contains the specified text, title bar caption, and response buttons.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <returns>Returns DialogResult.OK.</returns>
        public DialogResult Show(string message)
        {
            MessageBox.Show(message);
            return DialogResult.OK ;
        }

        /// <summary>
        /// Displays a message box that contains the specified text, title bar caption, and response buttons.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="caption">The title of the message box.</param>
        /// <param name="button"> A value that indicates the button or buttons to display.</param>
        /// <returns>A value that indicates the user's response to the message.</returns>
        public DialogResult Show(string message, string caption, DialogButton button)
        {
            return GetDialogResult(MessageBox.Show(message, caption, GetMessageBoxButton(button)));
        }

        DialogResult GetDialogResult(MessageBoxResult result)
        {
            if (result == MessageBoxResult.OK) return DialogResult.OK;
            if (result == MessageBoxResult.Cancel) return DialogResult.Cancel;
            throw new ArgumentOutOfRangeException("result");
        }

        MessageBoxButton GetMessageBoxButton(DialogButton button)
        {
            if (button == DialogButton.OK) return MessageBoxButton.OK;
            if (button == DialogButton.OKCancel) return MessageBoxButton.OKCancel;
            throw new ArgumentOutOfRangeException("button");
        }
    }
}