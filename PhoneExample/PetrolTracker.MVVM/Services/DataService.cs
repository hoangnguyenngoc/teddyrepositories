﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using PetrolTracker.Model;
using PetrolTracker.Mvvm.Adapters;

namespace PetrolTracker.Mvvm.Services 
{
    public class DataService : IDataService
    {
        readonly IIsolatedStorageFacade isolatedStorageFacade;

        Fleet fleet;

        /// <summary>
        /// Gets the fleet.
        /// </summary>
        public Fleet Fleet
        {
            get
            {
                if (fleet != null)
                {
                    return fleet;
                }
                fleet = isolatedStorageFacade.GetFleet();

                //this provides fleet initialization after being restored from isolated storage
                fleet.SetCurrentVehicle(fleet.CurrentVehicleId);
                LoadVehicleImages();
                return fleet;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataService"/> class.
        /// </summary>
        /// <param name="isolatedStorageFacade">The isolated storage facade.</param>
        public DataService(IIsolatedStorageFacade isolatedStorageFacade)
        {
            if (isolatedStorageFacade == null) throw new ArgumentNullException("isolatedStorageFacade");
            this.isolatedStorageFacade = isolatedStorageFacade;
        }

        /// <summary>
        /// Loads the vehicle image files from isolated storage into memory 
        /// </summary>
        void LoadVehicleImages()
        {
            foreach (var vehicle in Fleet.Vehicles)
            {
                vehicle.Picture = isolatedStorageFacade.GetVehicleImage(vehicle.PictureFileName);
            }
        }
    }
}