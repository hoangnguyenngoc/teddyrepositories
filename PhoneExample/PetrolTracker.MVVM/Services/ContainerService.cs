﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Windows;
using Funq;
using PetrolTracker.Mvvm.Adapters;
using PetrolTracker.Mvvm.ViewModel;

namespace PetrolTracker.Mvvm.Services
{
    /// <summary>
    /// Represents a container service that wraps the Funq container
    /// </summary>
    public class ContainerService : IDisposable
    {
        bool disposed;

        /// <summary>
        /// Gets the container.
        /// </summary>
        public Container Container { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:PetrolTracker.Mvvm.Services.ContainerService"/> class.
        /// </summary>
        public ContainerService()
        {
            this.Container = new Container();
            this.ConfigureContainer();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                this.Container.Dispose();
            }

            disposed = true;
        }

        void ConfigureContainer()
        {
            this.Container.Register<INavigationServiceFacade>(c => new NavigationServiceFacade(((App)Application.Current).RootFrame));
            this.Container.Register<IIsolatedStorageFacade>(c => new IsolatedStorageFacade());
            this.Container.Register<IDataService>(c => new DataService(c.Resolve<IIsolatedStorageFacade>()));
            this.Container.Register<IDialogService>(c => new DialogService());
            this.Container.Register(
                c => new VehicleHistoryViewModel(
                    c.Resolve<IDataService>(),
                    c.Resolve<INavigationServiceFacade>(),
                    c.Resolve<IIsolatedStorageFacade>()
                    )).ReusedWithin(ReuseScope.None);

            this.Container.Register(
                c => new SettingsViewModel(
                    c.Resolve<IDataService>(),
                    c.Resolve<INavigationServiceFacade>()
                    )).ReusedWithin(ReuseScope.None);

            this.Container.Register(
                c => new AddFillUpViewModel(
                    c.Resolve<IDataService>(),
                    c.Resolve<INavigationServiceFacade>(),
                    c.Resolve<IIsolatedStorageFacade>(),
                    c.Resolve<IDialogService>()
                    )).ReusedWithin(ReuseScope.None);
        }
    }
}