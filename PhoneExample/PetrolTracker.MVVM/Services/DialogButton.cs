﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


namespace PetrolTracker.Mvvm.Services
{
    /// <summary>
    /// Specifies the buttons to include when you display a message box.
    /// </summary>
    public enum DialogButton
    {
        /// <summary>
        /// Displays only the OK button.
        /// </summary>
        OK,
        /// <summary>
        /// Displays both the OK and Cancel buttons.
        /// </summary>
        OKCancel
    }
}
