﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


namespace PetrolTracker.Mvvm.Services
{
    /// <summary>
    /// Represents a user's response to a message box.
    /// </summary>
    public enum DialogResult
    {
        /// <summary>
        /// The user clicked the OK button.
        /// </summary>
        OK,
        /// <summary>
        /// The user clicked the Cancel button.
        /// </summary>
        Cancel
    }
}
