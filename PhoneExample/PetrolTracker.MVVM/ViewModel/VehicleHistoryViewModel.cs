﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PetrolTracker.Common;
using PetrolTracker.Common.Infrastructure;
using PetrolTracker.Model;
using PetrolTracker.Mvvm.Adapters;
using PetrolTracker.Mvvm.Services;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;

namespace PetrolTracker.Mvvm.ViewModel
{
    public class VehicleHistoryViewModel : NotificationObject
    {
        const string PivotSelectedIndexKey = "PivotSelectedIndexKey";

        readonly IDataService dataService;
        readonly INavigationServiceFacade navigationServiceFacade;
        readonly IIsolatedStorageFacade isolatedStorageFacade;
        string activeChartName;
        ICommand addFillUpCommand;
        ICommand costChartSelectedCommand;
        bool isNewInstance;
        ICommand mpgChartSelectedCommand;
        int selectedPivotPage;
        ICommand settingsCommand;

        public string ActiveChartName
        {
            get { return activeChartName; }
            set
            {
                activeChartName = value;
                RaisePropertyChanged("ActiveChartName");
            }
        }

        public ICommand AddFillUpCommand
        {
            get { return addFillUpCommand ?? (addFillUpCommand = new DelegateCommand(AddFillUpExecute)); }
        }

        public ICommand CostChartSelectedCommand
        {
            get { return costChartSelectedCommand ?? (costChartSelectedCommand = new DelegateCommand(CostChartSelectedExecute)); }
        }

        public Vehicle CurrentVehicle
        {
            get { return dataService.Fleet.CurrentVehicle; }
        }

        public IList<FillUp> FilteredFillUps
        {
            get { return dataService.Fleet.CurrentVehicle.FillUps.Where(f => f.QuantityUnitsPurchased > 0).OrderByDescending(f => f.DatePurchased).ThenByDescending(f => f.OdometerReading).ToList(); }
        }

        public ICommand MpgChartSelectedCommand
        {
            get { return mpgChartSelectedCommand ?? (mpgChartSelectedCommand = new DelegateCommand(MpgChartSelectedExecute)); }
        }

        public int SelectedPivotPage
        {
            get { return selectedPivotPage; }
            set
            {
                selectedPivotPage = value;
                RaisePropertyChanged("SelectedPivotPage");
            }
        }

        public ICommand SettingsCommand
        {
            get { return settingsCommand ?? (settingsCommand = new DelegateCommand(SettingsExecute)); }
        }

        public VehicleHistoryViewModel(IDataService dataService, INavigationServiceFacade navigationServiceFacade, IIsolatedStorageFacade isolatedStorageFacade)
        {
            if (dataService == null) throw new ArgumentNullException("dataService");
            if (navigationServiceFacade == null) throw new ArgumentNullException("navigationServiceFacade");
            if (isolatedStorageFacade == null) throw new ArgumentNullException("isolatedStorageFacade");
            this.dataService = dataService;
            this.navigationServiceFacade = navigationServiceFacade;
            this.isolatedStorageFacade = isolatedStorageFacade;
            isNewInstance = true;
            dataService.Fleet.CurrentVehicleChanged += (s, e) => SetCurrentVehicle();
            dataService.Fleet.CurrentVehicleFillUpAdded += (s, e) => RefreshFillUps();
        }

        public void NavigatedTo(bool isBeingActived, IDictionary<string, object> state)
        {
            if (isNewInstance)
            {
                // this demonstrates an application setting that survives application restarts
                this.ActiveChartName = isolatedStorageFacade.GetActiveChartName(Constants.MpgChartStateName);

                // this demonstrates a page setting that survives tombstoning
                if (isBeingActived && state.Count > 0)
                {
                    var pageStateHelper = new PageStateHelper(state);
                    this.SelectedPivotPage = pageStateHelper.GetValue(PivotSelectedIndexKey, 0);
                }
            }
            isNewInstance = false;
        }

        public void NavigatedFrom(IDictionary<string, object> pageState)
        {
            pageState[PivotSelectedIndexKey] = this.SelectedPivotPage;
        }

        void AddFillUpExecute()
        {
            navigationServiceFacade.Navigate(new Uri("/View/AddFillUpView.xaml", UriKind.Relative));
        }

        void SettingsExecute()
        {
            navigationServiceFacade.Navigate(new Uri("/View/SettingsView.xaml", UriKind.Relative));
        }

        void MpgChartSelectedExecute()
        {
            this.ActiveChartName = Constants.MpgChartStateName;
            isolatedStorageFacade.SaveActiveChartName(Constants.MpgChartStateName);
        }

        void CostChartSelectedExecute()
        {
            this.ActiveChartName = Constants.CostChartStateName;
            isolatedStorageFacade.SaveActiveChartName(Constants.CostChartStateName);
        }

        void SetCurrentVehicle()
        {
            RaisePropertyChanged("CurrentVehicle");
            RefreshFillUps();
        }

        void RefreshFillUps()
        {
            RaisePropertyChanged("FilteredFillUps");
        }
    }
}