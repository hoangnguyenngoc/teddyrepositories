﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.Generic;
using PetrolTracker.Model;
using PetrolTracker.Mvvm.Adapters;
using PetrolTracker.Mvvm.Services;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using System.Windows.Input;

namespace PetrolTracker.Mvvm.ViewModel
{
    public class SettingsViewModel : NotificationObject
    {
        readonly IDataService dataService;
        readonly INavigationServiceFacade navigationServiceFacade;
        bool isNewInstance;
        ICommand vehicleSelectedCommand;
        Vehicle selectedVehicle;

        public Vehicle SelectedVehicle 
        {
            get { return selectedVehicle; }
            private set
            {
                selectedVehicle = value;
                RaisePropertyChanged("SelectedVehicle");
            }
        }

        public ICommand VehicleSelectedCommand 
        {
            get { return vehicleSelectedCommand ?? (vehicleSelectedCommand = new DelegateCommand<Vehicle>(VehicleSelectedExecute)); }
        }

        public IList<Vehicle> Vehicles 
        {
            get { return dataService.Fleet.Vehicles; }
        }

        public SettingsViewModel(IDataService dataService, INavigationServiceFacade navigationServiceFacade) 
        {
            if(dataService == null) throw new ArgumentNullException("dataService");
            if(navigationServiceFacade == null) throw new ArgumentNullException("navigationServiceFacade");
            this.dataService = dataService;
            this.navigationServiceFacade = navigationServiceFacade;
            isNewInstance = true;
        }

        public void NavigatedTo() 
        {
            if(isNewInstance)
            {
                SetSelectedVehicle();
            }
            isNewInstance = false;
        }

        void VehicleSelectedExecute(Vehicle vehicle)
        {
            if(vehicle != null)
            {
                dataService.Fleet.SetCurrentVehicle(vehicle.VehicleId);
                navigationServiceFacade.GoBack();
            }
        }

        void SetSelectedVehicle()
        {
            this.SelectedVehicle = dataService.Fleet.CurrentVehicle;
        }
    }
}
