﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using PetrolTracker.Mvvm.Services;

namespace PetrolTracker.Mvvm.ViewModel 
{
    /// <summary>
    /// Represents a view model locator
    /// </summary>
    public class ViewModelLocator : IDisposable 
    {
        readonly ContainerService containerService;
        bool disposed;

        /// <summary>
        /// Gets the add fill up view model.
        /// </summary>
        public AddFillUpViewModel AddFillUpViewModel
        {
            get { return containerService.Container.Resolve<AddFillUpViewModel>(); }
        }

        /// <summary>
        /// Gets the vehicle history view model.
        /// </summary>
        public VehicleHistoryViewModel VehicleHistoryViewModel 
        {
            get { return containerService.Container.Resolve<VehicleHistoryViewModel>(); }
        }

        /// <summary>
        /// Gets the settings view model.
        /// </summary>
        public SettingsViewModel SettingsViewModel
        {
            get { return containerService.Container.Resolve<SettingsViewModel>(); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:PetrolTracker.Mvvm.ViewModel.ViewModelLocator"/> class.
        /// </summary>
        public ViewModelLocator() 
        {
            containerService = new ContainerService();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) 
            {
                return;
            }

            if (disposing)
            {
                containerService.Dispose();
            }

            disposed = true;
        }
    }
}