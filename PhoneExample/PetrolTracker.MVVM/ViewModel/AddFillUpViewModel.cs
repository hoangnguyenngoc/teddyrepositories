﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.Generic;
using System.Windows.Input;
using PetrolTracker.Common.Infrastructure;
using PetrolTracker.Model;
using PetrolTracker.Mvvm.Adapters;
using PetrolTracker.Mvvm.Services;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;

namespace PetrolTracker.Mvvm.ViewModel
{
    public class AddFillUpViewModel : NotificationObject
    {
        const string PurchaseDateKey = "PurchaseDateKey";
        const string OdometerKey = "OdometerKey";
        const string GallonsKey = "GallonsKey";
        const string PricePerGallonKey = "PricePerGallonKey";
        readonly IDataService dataService;
        readonly IDialogService dialogService;
        readonly IIsolatedStorageFacade isolatedStorageFacade;
        readonly INavigationServiceFacade navigationServiceFacade;
        string fillUpMpg;
        string gallons;
        bool isNewInstance;
        string milesDriven;
        string odometer;
        string pricePerGallon;
        DateTime? purchaseDate;
        ICommand saveCommand;
        string totalPurchase;

        public Vehicle CurrentVehicle
        {
            get { return dataService.Fleet.CurrentVehicle; }
        }

        public string FillUpMpg
        {
            get { return fillUpMpg; }
            private set
            {
                fillUpMpg = value;
                RaisePropertyChanged("FillUpMpg");
            }
        }

        public string Gallons
        {
            get { return gallons; }
            set
            {
                if (gallons == value) return;
                gallons = value;
                RaisePropertyChanged("Gallons");
                CalculateReadOnlyFields();
            }
        }

        public string MilesDriven 
        {
            get { return milesDriven; }
            private set
            {
                milesDriven = value;
                RaisePropertyChanged("MilesDriven");
            }
        }

        public string Odometer
        {
            get { return odometer; }
            set 
            {
                if (odometer == value) return;
                odometer = value;
                RaisePropertyChanged("Odometer");
                CalculateReadOnlyFields();
            }
        }

        public string PricePerGallon
        {
            get { return pricePerGallon; }
            set
            {
                if (pricePerGallon == value) return;
                pricePerGallon = value;
                RaisePropertyChanged("PricePerGallon");
                CalculateReadOnlyFields();
            }
        }

        public DateTime? PurchaseDate
        {
            get { return purchaseDate; }
            set
            {
                purchaseDate = value;
                RaisePropertyChanged("PurchaseDate");
            }
        }

        public ICommand SaveCommand
        {
            get { return saveCommand ?? (saveCommand = new DelegateCommand(SaveExecute)); }
        }

        public string TotalPurchase
        {
            get { return totalPurchase; }
            private set
            {
                totalPurchase = value;
                RaisePropertyChanged("TotalPurchase");
            }
        }

        public AddFillUpViewModel(IDataService dataService, INavigationServiceFacade navigationServiceFacade, IIsolatedStorageFacade isolatedStorageFacade, IDialogService dialogService)
        {
            if (dataService == null) throw new ArgumentNullException("dataService");
            if (navigationServiceFacade == null) throw new ArgumentNullException("navigationServiceFacade");
            if (isolatedStorageFacade == null) throw new ArgumentNullException("isolatedStorageFacade");
            if (dialogService == null) throw new ArgumentNullException("dialogService");
            this.dataService = dataService;
            this.navigationServiceFacade = navigationServiceFacade;
            this.isolatedStorageFacade = isolatedStorageFacade;
            this.dialogService = dialogService;
            isNewInstance = true;
            purchaseDate = GetTodayWithoutTime();
        }

        public void NavigatedTo(bool isBeingActivated, IDictionary<string, object> state)
        {
            // this demonstrates page settings that survives tombstonning
            if (isNewInstance && isBeingActivated && state.Count > 0)
            {
                var pageStateHelper = new PageStateHelper(state);
                this.PurchaseDate = pageStateHelper.GetValue<DateTime?>(PurchaseDateKey, DateTime.Now);
                this.Odometer = pageStateHelper.GetValue(OdometerKey, string.Empty);
                this.Gallons = pageStateHelper.GetValue(GallonsKey, string.Empty);
                this.PricePerGallon = pageStateHelper.GetValue(PricePerGallonKey, string.Empty);
                CalculateReadOnlyFields();
            }
            isNewInstance = false;
        }

        public void NavigatedFrom(IDictionary<string, object> state)
        {
            state[PurchaseDateKey] = this.PurchaseDate;
            state[OdometerKey] = this.Odometer;
            state[GallonsKey] = this.Gallons;
            state[PricePerGallonKey] = this.PricePerGallon;
        }

        public bool CancelBackKeyNavigation() 
        {
            if (string.IsNullOrWhiteSpace(this.PricePerGallon) && string.IsNullOrWhiteSpace(this.Odometer) && string.IsNullOrWhiteSpace(this.Gallons) && this.PurchaseDate.HasValue && this.PurchaseDate.Value == GetTodayWithoutTime())
            {
                return false;
            }
            return dialogService.Show("You have unsaved changes, continue?", "Unsaved Data", DialogButton.OKCancel) == DialogResult.Cancel;
        }

        void SaveExecute() 
        {
            var errors = new List<string>();
            int odometerReading;
            double pricePerUnitPurchased;
            double quantityUnitsPurchased;

            // sanity check for nulls, should not happen.
            if (!this.PurchaseDate.HasValue)
            {
                errors.Add("Purchase date is required.");
            }

            // locate any type mismatch errors, example an "a" in a numeric field.
            // note: this is not object validation

            if (!int.TryParse(this.Odometer, out odometerReading)) 
            {
                errors.Add("Odometer is not a valid whole number.");
            }

            if (!double.TryParse(this.Gallons, out quantityUnitsPurchased))
            {
                errors.Add("Gallons is not a valid number.");
            }

            if (!double.TryParse(this.PricePerGallon, out pricePerUnitPurchased)) 
            {
                errors.Add("Price per gallon is not a valid number.");
            }

            if (errors.Count > 0)
            {
                dialogService.Show(string.Join(Environment.NewLine, errors.ToArray()), "Invalid Input", DialogButton.OK);
                return;
            }

            // create and populate a new fill up object
            // Note: the PurchaseDate.Value has already been checked for null condition
            var fillUp = new FillUp
                            {
                                DatePurchased = this.PurchaseDate.Value,
                                MilesDriven = odometerReading - dataService.Fleet.CurrentVehicle.CurrentOdometer,
                                OdometerReading = odometerReading,
                                PricePerUnitPurchased = pricePerUnitPurchased,
                                QuantityUnitsPurchased = quantityUnitsPurchased
                            };

            // validate new fill up object
            // fillUp.Validate returns a collection of strings for any errors.
            errors = new List<string>(fillUp.Validate(dataService.Fleet.CurrentVehicle.CurrentOdometer));

            if (errors.Count > 0) 
            {
                dialogService.Show(string.Join(Environment.NewLine, errors.ToArray()), "Invalid Input", DialogButton.OK);
                return;
            }

            // if no errors then add to fill ups collection and persist the data to application storage
            dataService.Fleet.CurrentVehicle.FillUps.Add(fillUp);
            isolatedStorageFacade.SaveFleet(dataService.Fleet);
            navigationServiceFacade.GoBack();
        }

        DateTime GetTodayWithoutTime() 
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        void CalculateReadOnlyFields() 
        {
            this.FillUpMpg = string.Empty;
            this.MilesDriven = string.Empty;
            this.TotalPurchase = string.Empty;

            int odometerReading;
            int milesDriven = 0;
            double pricePerUnitPurchased;
            double quantityUnitsPurchased;

            int.TryParse(this.Odometer, out odometerReading);
            double.TryParse(this.Gallons, out quantityUnitsPurchased);
            double.TryParse(this.PricePerGallon, out pricePerUnitPurchased);

            // the below logic allows for none, some, or all read only values to be displayed.

            if (odometerReading > dataService.Fleet.CurrentVehicle.CurrentOdometer)
            {
                milesDriven = odometerReading - dataService.Fleet.CurrentVehicle.CurrentOdometer;
                this.MilesDriven = string.Format("{0:N0}", milesDriven);
            }

            if (milesDriven > 0 && quantityUnitsPurchased > 0) 
            {
                this.FillUpMpg = string.Format("{0:N1}", milesDriven / quantityUnitsPurchased);
            }

            if (pricePerUnitPurchased > 0 && quantityUnitsPurchased > 0)
            {
                this.TotalPurchase = string.Format("{0:N2}", pricePerUnitPurchased * quantityUnitsPurchased);
            }
        }
    }
}