﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PetrolTracker.Model.Tests
{
    [TestClass]
    public class VehicleFixture 
    {
        [TestMethod]
        public void AddingItemToFillUpsPopulatesVehicleProperties()
        {
            // arrange
            const double expectedMpg = 17.1d;
            const int odometer = 800;
            FillUp passInEventArgs = null;
            bool fillUpAddedEventRaised = false;
            DateTime dateFilledUp = DateTime.Today;
            var target = new Vehicle { InitialOdometer = 500 };
            var fillUp = new FillUp { DatePurchased = dateFilledUp, MilesDriven = 300, OdometerReading = odometer, PricePerUnitPurchased = 3.95d, QuantityUnitsPurchased = 17.5d };

            target.FillUpAdded += (s, e) => 
                {
                    fillUpAddedEventRaised = true;
                    passInEventArgs = e.FillUp;
                };

            // act
            target.FillUps.Add(fillUp);

            // assert
            Assert.IsTrue(fillUpAddedEventRaised);
            Assert.AreSame(fillUp, passInEventArgs);
            Assert.AreEqual(dateFilledUp, target.LastFillUpDate);
            Assert.AreEqual(odometer, target.CurrentOdometer);
            Assert.AreEqual(expectedMpg, target.AverageMpg);
        }
    }
}
