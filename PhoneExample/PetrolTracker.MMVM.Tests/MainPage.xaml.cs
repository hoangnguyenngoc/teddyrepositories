﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using Microsoft.Phone.Controls;
using Microsoft.Silverlight.Testing;

namespace PetrolTracker.Mvvm.Tests 
{
    public partial class MainPage : PhoneApplicationPage 
    {
        public MainPage() 
        {
            InitializeComponent();

            Content = UnitTestSystem.CreateTestPage();
        }
    }
}