﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using PetrolTracker.Model;
using PetrolTracker.Mvvm.ViewModel;
using PetrolTracker.Mvvm.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PetrolTracker.Mvvm.Tests.ViewModel
{
    [TestClass]
    public class VehicleHistoryViewModelFixture
    {
        [TestMethod]
        public void RefreshesListWhenFillUpAdded()
        {
            // arrange
            var vehicle = new Vehicle { VehicleId = Guid.NewGuid() };
            var dataService = new MockDataService();
            var navigationService = new MockNavigationServiceFacade { GoBackCalled = false };
            var isolatedStorage = new MockIsolatedStorageFacade();

            var target = new VehicleHistoryViewModel(dataService, navigationService, isolatedStorage);

            // act
            vehicle.FillUps.Add(new FillUp()
            {
                DatePurchased = DateTime.Now,
                MilesDriven = 300,
                OdometerReading = 10000,
                PricePerUnitPurchased = 3.64,
                QuantityUnitsPurchased = 12
            });

            dataService.Fleet.Vehicles.Add(vehicle);
            dataService.Fleet.SetCurrentVehicle(vehicle.VehicleId);

            // assert
            Assert.IsTrue(target.FilteredFillUps.Count == 1);

            vehicle.FillUps.Add(new FillUp()
            {
                DatePurchased = DateTime.Now,
                MilesDriven = 200,
                OdometerReading = 10300,
                PricePerUnitPurchased = 3.64,
                QuantityUnitsPurchased = 8
            });

            Assert.IsTrue(target.FilteredFillUps.Count == 2);
        }

        [TestMethod]
        public void ShowsFillUps()
        {
            // arrange
            var vehicle = new Vehicle { VehicleId = Guid.NewGuid() };
            var dataService = new MockDataService();
            var navigationService = new MockNavigationServiceFacade { GoBackCalled = false };
            var isolatedStorage = new MockIsolatedStorageFacade();

            var target = new VehicleHistoryViewModel(dataService, navigationService, isolatedStorage);

            // act
            vehicle.FillUps.Add(new FillUp()
            {
                DatePurchased = DateTime.Now,
                MilesDriven = 300,
                OdometerReading = 10000,
                PricePerUnitPurchased = 3.64,
                QuantityUnitsPurchased = 12
            });

            dataService.Fleet.Vehicles.Add(vehicle);
            dataService.Fleet.SetCurrentVehicle(vehicle.VehicleId);

            // assert
            Assert.IsTrue(target.FilteredFillUps.Count == 1);
        }
    }
}
