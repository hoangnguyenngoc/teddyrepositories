﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Linq;
using PetrolTracker.Model;
using PetrolTracker.Mvvm.Tests.Mocks;
using PetrolTracker.Mvvm.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PetrolTracker.Mvvm.Tests.ViewModel
{
    [TestClass]
    public class AddFillUpViewModelFixture
    {
        [TestMethod]
        public void SaveFillUpAndNavigatesBack()
        {
            // arrange
            var vehicle = new Vehicle { VehicleId = Guid.NewGuid() };
            var dataService = new MockDataService();
            var navigationService = new MockNavigationServiceFacade { GoBackCalled = false };
            var isolatedStorage = new MockIsolatedStorageFacade();
            var dialogService = new MockDialogService();
            dataService.Fleet.Vehicles.Add(vehicle);
            dataService.Fleet.SetCurrentVehicle(vehicle.VehicleId);

            var purchaseDateValue = DateTime.Now;
            var odometerValue = "10000";
            var gallonesValue = "12";
            var pricePerGallonValue = "3.64";

            var target = new AddFillUpViewModel(dataService, navigationService, isolatedStorage, dialogService);
            target.PurchaseDate = purchaseDateValue;
            target.Odometer = odometerValue;
            target.Gallons = gallonesValue;
            target.PricePerGallon = pricePerGallonValue;

            // act
            target.SaveCommand.Execute(null);

            // assert
            Assert.IsTrue(dataService.Fleet.CurrentVehicle.FillUps.Count > 0);
            
            var savedfillUp = dataService.Fleet.CurrentVehicle.FillUps.Last();
            Assert.AreEqual(savedfillUp.DatePurchased, purchaseDateValue);
            Assert.AreEqual(savedfillUp.OdometerReading, int.Parse(odometerValue));
            Assert.AreEqual(savedfillUp.QuantityUnitsPurchased, int.Parse(gallonesValue));
            Assert.AreEqual(savedfillUp.PricePerUnitPurchased, double.Parse(pricePerGallonValue));

            Assert.IsTrue(navigationService.GoBackCalled);
        }

        [TestMethod]
        public void SaveFillUpAndShowDialogOnError()
        {
            // arrange
            var vehicle = new Vehicle { VehicleId = Guid.NewGuid() };
            var dataService = new MockDataService();
            var navigationService = new MockNavigationServiceFacade { GoBackCalled = false };
            var isolatedStorage = new MockIsolatedStorageFacade();
            var dialogService = new MockDialogService() { ShowCalled = false }; ;
            dataService.Fleet.Vehicles.Add(vehicle);
            dataService.Fleet.SetCurrentVehicle(vehicle.VehicleId);

            var purchaseDateValue = DateTime.Now;
            var odometerValue = "10000";
            var gallonesValue = "12";
            var pricePerGallonValue = "too much";

            var target = new AddFillUpViewModel(dataService, navigationService, isolatedStorage, dialogService);
            target.PurchaseDate = purchaseDateValue;
            target.Odometer = odometerValue;
            target.Gallons = gallonesValue;
            target.PricePerGallon = pricePerGallonValue;

            // act
            target.SaveCommand.Execute(null);

            // assert
            Assert.IsTrue(dialogService.ShowCalled);
        }
    }
}
