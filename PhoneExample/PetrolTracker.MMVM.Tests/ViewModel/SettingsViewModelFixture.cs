﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using PetrolTracker.Model;
using PetrolTracker.Mvvm.Tests.Mocks;
using PetrolTracker.Mvvm.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PetrolTracker.Mvvm.Tests.ViewModel
{
    [TestClass]
    public class SettingsViewModelFixture
    {
        [TestMethod]
        public void SelectingVehicleSetFleetCurrentVehicleAndNavigatesBack()
        {
            // arrange
            var targetVehicle = new Vehicle { VehicleId = Guid.NewGuid() };
            var extraVehicle = new Vehicle { VehicleId = Guid.NewGuid() };
            var dataService = new MockDataService();
            var navigationService = new MockNavigationServiceFacade { GoBackCalled = false };
            var target = new SettingsViewModel(dataService, navigationService);
            dataService.Fleet.Vehicles.Add(targetVehicle);
            dataService.Fleet.Vehicles.Add(extraVehicle);

            // act
            target.VehicleSelectedCommand.Execute(targetVehicle);

            // assert
            Assert.AreSame(targetVehicle, dataService.Fleet.CurrentVehicle);
            Assert.IsTrue(navigationService.GoBackCalled);
        }
    }
}
