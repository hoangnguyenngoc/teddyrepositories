﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using PetrolTracker.Model;
using PetrolTracker.Mvvm.Services;

namespace PetrolTracker.Mvvm.Tests.Mocks
{
    public class MockDataService : IDataService
    {
        public Fleet Fleet { get; set; }

        public MockDataService()
        {
            this.Fleet = new Fleet();
        }
    }
}
