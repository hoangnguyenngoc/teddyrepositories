﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using PetrolTracker.Mvvm.Adapters;

namespace PetrolTracker.Mvvm.Tests.Mocks
{
    public class MockNavigationServiceFacade : INavigationServiceFacade
    {
        public Uri NavigateCalledWithUri { get; set; }
        public bool GoBackCalled { get; set; }
        public bool CanGoBack { get; set; }

        public Uri CurrentSource 
        {
            get { throw new NotImplementedException(); }
        }

        public bool Navigate(Uri source) 
        {
            this.NavigateCalledWithUri = source;
            return true;
        }

        public void GoBack()
        {
            this.GoBackCalled = true;
        }
    }
}
