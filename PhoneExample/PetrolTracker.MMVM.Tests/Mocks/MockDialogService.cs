﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using PetrolTracker.Mvvm.Services;

namespace PetrolTracker.Mvvm.Tests.Mocks
{
    public class MockDialogService : IDialogService
    {
        public bool ShowCalled { get; set; }

        public DialogResult Show(string message)
        {
            ShowCalled = true;
            return DialogResult.OK;
        }

        public DialogResult Show(string message, string caption, DialogButton button)
        {
            ShowCalled = true;
            return DialogResult.OK;
        }
    }
}
