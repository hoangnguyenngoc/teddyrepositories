﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using PetrolTracker.Mvvm.Adapters;
using System.Windows.Media.Imaging;

namespace PetrolTracker.Mvvm.Tests.Mocks
{
    public class MockIsolatedStorageFacade :IIsolatedStorageFacade
    {

        public PetrolTracker.Model.Fleet GetFleet()
        {
            return null;
        }

        public void SaveFleet(PetrolTracker.Model.Fleet fleet)
        {
        }

        public BitmapImage GetVehicleImage(string vehicleImageName)
        {
            return null;
        }

        public string GetActiveChartName(string defaultName)
        {
            return string.Empty;
        }

        public void SaveActiveChartName(string name)
        {
        }
    }
}
