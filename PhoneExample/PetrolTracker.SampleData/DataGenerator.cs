﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Media.Imaging;
using PetrolTracker.Model;

namespace PetrolTracker.SampleData 
{
    /// <summary>
    /// Represents a data generator.
    /// </summary>
    public class DataGenerator : IDataGenerator
    {       
        /// <summary>
        /// Initializes a new instance of the <see cref="DataGenerator"/> class.
        /// </summary>
        public DataGenerator() 
        {
        }

        /// <summary>
        /// Creates the fleet.
        /// </summary>
        /// <returns>A populated fleet</returns>
        /// <remarks>Creates a fleet of vehicles and copies each vehicle's image to isolated storage along with the fleet object graph.</remarks>
        public Fleet CreateFleet()
        {
            var fleet = new Fleet();
            fleet.Vehicles.Add(CreateVehicle("HOT ROD", "/Images/hotrod.png", 250, 2, 17, 4.10, 17.5));
            fleet.Vehicles.Add(CreateVehicle("MOTORCYCLE", "/Images/motorcycle.png", 10512, 1, 5, 3.65, 35.2));
            fleet.Vehicles.Add(CreateVehicle("RACECAR", "/Images/racecar.png", 25, 3, 22, 4.70, 6.5));
            return fleet;
        }

        Vehicle CreateVehicle(string name, string sampleImageFileName, int initialOdometer, int fillUpsPerWeek, double tankCapactiy, double fuelPrice, double targetMpg)
        {
            var vehicle = new Vehicle {VehicleId = Guid.NewGuid(), VehicleName = name};

            var uri = new Uri(sampleImageFileName, UriKind.Relative);
            vehicle.Picture = new BitmapImage(uri);
            vehicle.PictureFileName = string.Concat(vehicle.VehicleId.ToString(), ".png");
            CopyImageToIsolatedStorage(sampleImageFileName, vehicle.PictureFileName);
            AddFillUpHistory(vehicle, initialOdometer, fillUpsPerWeek, tankCapactiy, fuelPrice, targetMpg);
            return vehicle;
        }

        void CopyImageToIsolatedStorage(string projectImageFileName, string isolatedStorageImageFileName)
        {
            using (var isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf.FileExists(isolatedStorageImageFileName))
                {
                    isf.DeleteFile(isolatedStorageImageFileName);
                }

                var pngStream = Application.GetResourceStream(new Uri(@"/PetrolTracker.SampleData;component/" + projectImageFileName, UriKind.Relative)).Stream;
                using (var isfs = new IsolatedStorageFileStream(isolatedStorageImageFileName, FileMode.Create, isf))
                {
                    int counter;
                    var buffer = new byte[1024];
                    while (0 < (counter = pngStream.Read(buffer, 0, buffer.Length)))
                    {
                        isfs.Write(buffer, 0, counter);
                    }
                    pngStream.Close();
                }
            }
        }

        void AddFillUpHistory(Vehicle vehicle, int initialOdometer, int fillUpsPerWeek, double tankCapactiy, double fuelPrice, double targetMpg)
        {
            var lastFillUpDate = DateTime.Today.AddDays(-7);
            var firstFillUpDate = lastFillUpDate.AddDays(-120);
            var fillUpDate = firstFillUpDate;
            var stepCount = GetStepCountForFillUpsPerWeek(fillUpsPerWeek);
            var random = new Random();
            var thisOdometer = initialOdometer;
            vehicle.FillUps.Add(
                new FillUp 
                    {
                        DatePurchased = firstFillUpDate,
                        MilesDriven = 0,
                        OdometerReading = thisOdometer,
                        PricePerUnitPurchased = 0,
                        QuantityUnitsPurchased = 0
                    });

            for (var i = 120; i > 0; i -= stepCount)
            {
                fillUpDate = fillUpDate.AddDays(stepCount);
                if (fillUpDate > lastFillUpDate)
                {
                    return;
                }

                var thisFuelPrice = fuelPrice + ((random.Next(-1, 1) * .1) / 2);
                var thisMpg = targetMpg + (random.Next(-3, 3));
                var thisQty = tankCapactiy + (random.Next(-3, 0) * .3);
                var thisMilesDriven = (int) (thisMpg * thisQty);
                thisOdometer += thisMilesDriven;
                vehicle.FillUps.Add(
                    new FillUp
                        {
                            DatePurchased = fillUpDate,
                            MilesDriven = thisMilesDriven,
                            OdometerReading = thisOdometer,
                            PricePerUnitPurchased = thisFuelPrice,
                            QuantityUnitsPurchased = thisQty
                        });
            }
        }

        int GetStepCountForFillUpsPerWeek(int fillUpsPerWeek) 
        {
            switch (fillUpsPerWeek)
            {
                case 1:
                    return 7;
                case 2:
                    return 3;
                case 3:
                case 4:
                case 5:
                    return 2;
                default:
                    return 1;
            }
        }
    }
}