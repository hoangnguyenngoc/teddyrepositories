﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


namespace PetrolTracker.Mvvm.Adapters {
    using System;
    using System.IO;
    using System.IO.IsolatedStorage;
    using System.Windows.Media.Imaging;
    using PetrolTracker.Common;
    using PetrolTracker.Model;

    /// <summary>
    /// Represents a simple applicaiton specific facade over isolated storage
    /// </summary>
    public class IsolatedStorageFacade : IIsolatedStorageFacade {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:PetrolTracker.Mvvm.Adapters.IsolatedStorageFacade"/> class.
        /// </summary>
        /// <remarks></remarks>
        public IsolatedStorageFacade() {
        }

        /// <summary>
        /// Gets the fleet from isolated storage.
        /// </summary>
        /// <exception cref="InvalidOperationException">If fleet is not in isolated storage.</exception>
        /// <returns>Fleet object</returns>
        public Fleet GetFleet() {
            if(!IsolatedStorageSettings.ApplicationSettings.Contains(Constants.FleetKey)) {
                throw new InvalidOperationException("fleet is missing from isolated storage.");
            }
            var fleet = (Fleet)IsolatedStorageSettings.ApplicationSettings[Constants.FleetKey];
            fleet.SetCurrentVehicle(fleet.CurrentVehicleId);
            return fleet;
        }

        /// <summary>
        /// Saves the fleet to isolated storge.
        /// </summary>
        /// <param name="fleet">The fleet.</param>
        /// <exception cref="ArgumentNullException">If fleet argument is null.</exception>
        public void SaveFleet(Fleet fleet) {
            if(fleet == null) throw new ArgumentNullException("fleet");
            IsolatedStorageSettings.ApplicationSettings[Constants.FleetKey] = fleet;
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        /// <summary>
        /// Reads the named file in isolated storage into BitmapImage object that is returned.
        /// </summary>
        /// <param name="vehicleImageName">Name of the vehicle image.</param>
        /// <exception cref="ArgumentException">If vehicleImageName argument is null or empty.</exception>
        /// <exception cref="FileNotFoundException">If the file vehicleImageName is not in isolated storage.</exception>
        /// <returns>BitmapImage for named file</returns>
        public BitmapImage GetVehicleImage(string vehicleImageName) {
            if(string.IsNullOrWhiteSpace(vehicleImageName)) throw new ArgumentException("vehicleImageName was null or empty string");

            var picture = new BitmapImage();
            using(var isf = IsolatedStorageFile.GetUserStoreForApplication()) {
                if(!isf.FileExists(vehicleImageName)) {
                    throw new FileNotFoundException(vehicleImageName);
                }
                using(IsolatedStorageFileStream fs = isf.OpenFile(vehicleImageName, FileMode.Open, FileAccess.Read)) {
                    picture.SetSource(fs);
                }
            }
            return picture;
        }

        /// <summary>
        /// Gets the active chart name from application settings in isolated storage
        /// </summary>
        /// <param name="defaultName">The default active chart name.</param>
        /// <returns>Returns the value in application settings; if no value returns the default value.</returns>
        public string GetActiveChartName(string defaultName) {
            if(!IsolatedStorageSettings.ApplicationSettings.Contains(Constants.ChartIndexKey)) {
                return defaultName;
            }
            return (string)IsolatedStorageSettings.ApplicationSettings[Constants.ChartIndexKey];
        }

        /// <summary>
        /// Saves the index of the history chart to application settings in isolated storage
        /// </summary>
        /// <param name="name">The name.</param>
        public void SaveActiveChartName(string name) {
            IsolatedStorageSettings.ApplicationSettings[Constants.ChartIndexKey] = name;
            IsolatedStorageSettings.ApplicationSettings.Save();
        }
    }
}