﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


namespace PetrolTracker.Mvvm.Adapters {
    using System;
    using System.IO;
    using System.Windows.Media.Imaging;
    using PetrolTracker.Model;

    public interface IIsolatedStorageFacade {
        /// <summary>
        /// Gets the fleet from isolated storage.
        /// </summary>
        /// <exception cref="InvalidOperationException">If fleet is not in isolated storage.</exception>
        /// <returns>Fleet object</returns>
        Fleet GetFleet();

        /// <summary>
        /// Saves the fleet to isolated storge.
        /// </summary>
        /// <param name="fleet">The fleet.</param>
        /// <exception cref="ArgumentNullException">If fleet argument is null.</exception>
        void SaveFleet(Fleet fleet);

        /// <summary>
        /// Reads the named file in isolated storage into BitmapImage object that is returned.
        /// </summary>
        /// <param name="vehicleImageName">Name of the vehicle image.</param>
        /// <exception cref="ArgumentException">If vehicleImageName argument is null or empty.</exception>
        /// <exception cref="FileNotFoundException">If the file vehicleImageName is not in isolated storage.</exception>
        /// <returns>BitmapImage for named file</returns>
        BitmapImage GetVehicleImage(string vehicleImageName);

        /// <summary>
        /// Gets the active chart name from application settings in isolated storage
        /// </summary>
        /// <param name="defaultName">The default active chart name.</param>
        /// <returns>Returns the value in application settings; if no value returns the default value.</returns>
        string GetActiveChartName(string defaultName);

        /// <summary>
        /// Saves the index of the history chart to application settings in isolated storage
        /// </summary>
        /// <param name="name">The name.</param>
        void SaveActiveChartName(string name);
    }
}