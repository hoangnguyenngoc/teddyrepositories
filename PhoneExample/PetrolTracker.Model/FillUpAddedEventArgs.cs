﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;

namespace PetrolTracker.Model 
{
    public class FillUpAddedEventArgs : EventArgs
    {
        readonly FillUp fillUp;

        /// <summary>
        /// Gets the fill up.
        /// </summary>
        public FillUp FillUp
        {
            get { return fillUp; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FillUpAddedEventArgs"/> class.
        /// </summary>
        /// <param name="fillUp">The fill up.</param>
        public FillUpAddedEventArgs(FillUp fillUp)
        {
            if (fillUp == null) throw new ArgumentNullException("fillUp");
            this.fillUp = fillUp;
        }
    }
}