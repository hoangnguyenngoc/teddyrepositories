﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.Generic;
using System.Linq;

namespace PetrolTracker.Model
{
    /// <summary>
    /// Represents a fleet, which is all the owners vehicles being tracked by this program.
    /// </summary>
    public class Fleet
    {
        /// <summary>
        /// Occurs when current vehicle is changed.
        /// </summary>
        public event EventHandler<CurrentVehicleChangedEventArgs> CurrentVehicleChanged;

        /// <summary>
        /// Occurs when a fill up is added to the current vehicle.
        /// </summary>
        public event EventHandler<FillUpAddedEventArgs> CurrentVehicleFillUpAdded;

        /// <summary>
        /// Gets the current vehicle.
        /// </summary>
        /// <value>The current vehicle.</value>
        public Vehicle CurrentVehicle { get; private set; }

        /// <summary>
        /// Gets or sets the current vehicle id.
        /// </summary>
        /// <value>The current vehicle id.</value>
        public Guid CurrentVehicleId { get; set; }

        /// <summary>
        /// Gets or sets the vehicles.
        /// </summary>
        /// <value>The vehicles being tracked by this program.</value>
        public IList<Vehicle> Vehicles { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Fleet"/> class.
        /// </summary>
        public Fleet() 
        {
            this.Vehicles = new List<Vehicle>();
        }

        /// <summary>
        /// Sets the current vehicle.
        /// </summary>
        /// <param name="vehicleId">The current vehicle id.</param>
        public void SetCurrentVehicle(Guid vehicleId)
        {
            this.CurrentVehicleId = vehicleId;
            this.CurrentVehicle = this.Vehicles.Where(v => v.VehicleId == vehicleId).Single();
            OnCurrentVehicleChanged(this.CurrentVehicle);
            this.CurrentVehicle.FillUpAdded -= CurrentVehicle_FillUpAdded;
            this.CurrentVehicle.FillUpAdded += CurrentVehicle_FillUpAdded;
        }

        void CurrentVehicle_FillUpAdded(object sender, FillUpAddedEventArgs e) 
        {
            OnCurrentVehicleFillUpAdded(e.FillUp);
        }

        /// <summary>
        /// Called when current vehicle changed.
        /// </summary>
        /// <param name="currentVehicle">The current vehicle.</param>
        protected void OnCurrentVehicleChanged(Vehicle currentVehicle)
        {
            var handler = this.CurrentVehicleChanged;
            if (handler != null)
            {
                handler(this, new CurrentVehicleChangedEventArgs(currentVehicle));
            }
        }

        /// <summary>
        /// Called when the current vehicle has a fill up added.
        /// </summary>
        /// <param name="fillUp">The added fill up.</param>
        protected void OnCurrentVehicleFillUpAdded(FillUp fillUp)
        {
            var handler = this.CurrentVehicleFillUpAdded;
            if(handler != null)
            {
                handler(this, new FillUpAddedEventArgs(fillUp));
            }
        }
    }
}