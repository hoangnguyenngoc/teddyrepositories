﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Media.Imaging;
using Microsoft.Practices.Prism.ViewModel;

namespace PetrolTracker.Model 
{
    /// <summary>
    /// Represents a vehicle
    /// </summary>
    public class Vehicle : NotificationObject
    {
        double averageMpg;
        int currentOdometer;
        ObservableCollection<FillUp> fillUps;
        int initialOdometer;
        DateTime lastFillUpDate;
        BitmapImage picture;
        string pictureFileName;
        Guid vehicleId;
        string _vehicleName;

        /// <summary>
        /// Occurs when a fill up is added.
        /// </summary>
        public event EventHandler<FillUpAddedEventArgs> FillUpAdded;
        
        /// <summary>
        /// Gets or sets the average MPG.
        /// </summary>
        /// <value>The average MPG based on all fill up entered.</value>
        public double AverageMpg
        {
            get { return averageMpg; }
            set
            {
                if (Math.Abs(averageMpg - value) < double.Epsilon) return;
                averageMpg = value;
                RaisePropertyChanged("AverageMpg");
            }
        }

        /// <summary>
        /// Gets or sets the current odometer.
        /// </summary>
        /// <value>The current odometer.</value>
        public int CurrentOdometer
        {
            get { return currentOdometer; }
            set
            {
                if (currentOdometer == value) return;
                currentOdometer = value;
                RaisePropertyChanged("CurrentOdometer");
            }
        }

        /// <summary>
        /// Gets or sets the fill ups.
        /// </summary>
        /// <value>The collection fill ups.</value>
        /// <remarks>When this collection changed event is raised, code runs that updates calculated properties on this object.</remarks>
        public ObservableCollection<FillUp> FillUps 
        {
            get { return fillUps; }
            set 
            {
                if (fillUps == value) return;
                fillUps = value;
                fillUps.CollectionChanged += FillUps_CollectionChanged;
                RaisePropertyChanged("FillUps");
            }
        }

        /// <summary>
        /// Gets or sets the initial odometer.
        /// </summary>
        /// <value>The initial odometer reading set when the first fill up was entered.</value>
        public int InitialOdometer 
        {
            get { return initialOdometer; }
            set 
            {
                if (initialOdometer == value) return;
                initialOdometer = value;
                RaisePropertyChanged("InitialOdometer");
            }
        }

        /// <summary>
        /// Gets or sets the last fill up date.
        /// </summary>
        /// <value>The last fill up date.</value>
        public DateTime LastFillUpDate
        {
            get { return lastFillUpDate; }
            set
            {
                lastFillUpDate = value;
                RaisePropertyChanged("LastFillUpDate");
            }
        }

        /// <summary>
        /// Gets or sets the picture.
        /// </summary>
        /// <value>The vehicle picture.</value>
        [IgnoreDataMember]
        public BitmapImage Picture
        {
            get { return picture; }
            set
            {
                if (picture == value) return;
                picture = value;
                RaisePropertyChanged("Picture");
            }
        }

        /// <summary>
        /// Gets or sets the name of the picture file.
        /// </summary>
        /// <value>The name of the picture file.</value>
        public string PictureFileName
        {
            get { return pictureFileName; }
            set
            {
                if (pictureFileName == value) return;
                pictureFileName = value;
                RaisePropertyChanged("PictureFileName");
            }
        }

        /// <summary>
        /// Gets or sets the vehicle id.
        /// </summary>
        /// <value>The vehicle id.</value>
        /// <remarks>The vehicle id is used to uniquely identify this vehicle and its associated image file on disk.</remarks>
        public Guid VehicleId
        {
            get { return vehicleId; }
            set
            {
                vehicleId = value;
                if (vehicleId == value) return;
                RaisePropertyChanged("VehicleId");
            }
        }

        /// <summary>
        /// Gets or sets the name of the vehicle.
        /// </summary>
        /// <value>The name of the vehicle.</value>
        public string VehicleName
        {
            get { return _vehicleName; }
            set
            {
                if (_vehicleName == value) return;
                _vehicleName = value;
                RaisePropertyChanged("VehicleName");
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vehicle"/> class.
        /// </summary>
        public Vehicle()
        {
            this.FillUps = new ObservableCollection<FillUp>();
        }

        void FillUps_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                var newFillUp = e.NewItems[e.NewItems.Count - 1] as FillUp;
                if (newFillUp != null) 
                {
                    this.LastFillUpDate = newFillUp.DatePurchased;
                    UpdateOdometer(newFillUp);
                    UpdateAverageMpg();
                    OnFillUpAdded(newFillUp);
                }
            }
        }

        /// <summary>
        /// Called when a fill up is added.
        /// </summary>
        /// <param name="fillUp">The fill up.</param>
        protected void OnFillUpAdded(FillUp fillUp)
        {
            var handler = this.FillUpAdded;
            if (handler != null)
            {
                handler(this, new FillUpAddedEventArgs(fillUp));
            }
        }

        void UpdateOdometer(FillUp fillUp)
        {
            if (fillUp == null) throw new ArgumentNullException("fillUp");
            if (this.InitialOdometer == 0)
            {
                this.InitialOdometer = fillUp.OdometerReading;
            }
            if (this.CurrentOdometer >= fillUp.OdometerReading)
            {
                throw new InvalidOperationException("fill up odometer reading must be greater than current odometer reading");
            }
            this.CurrentOdometer = fillUp.OdometerReading;
        }

        void UpdateAverageMpg()
        {
            double totalFuel = FillUps.Sum(f => f.QuantityUnitsPurchased);
            int totalDistance = FillUps.Sum(f => f.MilesDriven);
            if (totalDistance > 0 && totalFuel > 0)
            {
                this.AverageMpg = Math.Round(totalDistance / totalFuel, 1);
            }
        }
    }
}