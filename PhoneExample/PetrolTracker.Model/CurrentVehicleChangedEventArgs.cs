﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;

namespace PetrolTracker.Model
{
    public class CurrentVehicleChangedEventArgs : EventArgs
    {    
        readonly Vehicle currentVehicle;

        /// <summary>
        /// Gets the current vehicle.
        /// </summary>
        public Vehicle CurrentVehicle
        {
            get { return currentVehicle; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentVehicleChangedEventArgs"/> class.
        /// </summary>
        /// <param name="currentVehicle">The current vehicle.</param>
        public CurrentVehicleChangedEventArgs(Vehicle currentVehicle)
        {
            if (currentVehicle == null) throw new ArgumentNullException("currentVehicle");
            this.currentVehicle = currentVehicle;
        }
    }
}