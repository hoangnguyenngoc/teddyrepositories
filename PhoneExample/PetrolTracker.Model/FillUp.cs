﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.Generic;
using Microsoft.Practices.Prism.ViewModel;

namespace PetrolTracker.Model
{
    /// <summary>
    /// Represents a fill up
    /// </summary>
    public class FillUp : NotificationObject
    {
        DateTime datePurchased;
        int mileDriven;
        int odometerReading;
        double pricePerUnitPurchased;
        double quantityUnitsPurchased;

        /// <summary>
        /// Gets or sets the date purchased.
        /// </summary>
        /// <value>The date this fill up was purchased.</value>
        public DateTime DatePurchased
        {
            get { return datePurchased; }
            set
            {
                if(datePurchased == value) return;
                datePurchased = value;
                RaisePropertyChanged("DatePurchased");
            }
        }

        /// <summary>
        /// Gets or sets the miles driven.
        /// </summary>
        /// <value>The miles driven since the last fill up.</value>
        /// <remarks></remarks>
        public int MilesDriven
        {
            get { return mileDriven; }
            set
            {
                mileDriven = value;
                RaisePropertyChanged("MilesDriven");
                RaisePropertyChanged("Mpg");
            }
        }

        /// <summary>
        /// Gets the Mpg for this fill up.
        /// </summary>
        public double Mpg
        {
            get
            {
                if(this.QuantityUnitsPurchased > 0 && this.MilesDriven > 0)
                {
                    return Math.Round(this.MilesDriven / this.QuantityUnitsPurchased, 1);
                }
                return 0;
            }
        }

        public double TotalPurchased
        {
            get
            {
                return Math.Round(this.QuantityUnitsPurchased * this.PricePerUnitPurchased, 2);
            }
        }

        /// <summary>
        /// Gets or sets the odometer reading.
        /// </summary>
        /// <value>The odometer reading of the vehicle when this purchase was made.</value>
        public int OdometerReading
        {
            get { return odometerReading; }
            set
            {
                if(odometerReading == value) return;
                odometerReading = value;
                RaisePropertyChanged("OdometerReading");
                RaisePropertyChanged("Mpg");
            }
        }

        /// <summary>
        /// Gets or sets the price per unit purchased.
        /// </summary>
        /// <value>The price per unit of fuel that was purchased.</value>
        public double PricePerUnitPurchased
        {
            get { return pricePerUnitPurchased; }
            set
            {
                if(Math.Abs(pricePerUnitPurchased - value) < double.Epsilon) return;
                pricePerUnitPurchased = value;
                RaisePropertyChanged("PricePerUnitPurchased");
            }
        }

        /// <summary>
        /// Gets or sets the quantity units purchased.
        /// </summary>
        /// <value>The quantity units that were purchased.</value>
        public double QuantityUnitsPurchased
        {
            get { return quantityUnitsPurchased; }
            set
            {
                if(Math.Abs(quantityUnitsPurchased - value) < double.Epsilon) return;
                quantityUnitsPurchased = value;
                RaisePropertyChanged("QuantityUnitsPurchased");
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FillUp"/> class.
        /// </summary>
        public FillUp()
        {
        }

        public IList<string> Validate(int currentOdometer)
        {
            var errors = new List<string>();

            if(this.DatePurchased > DateTime.Now)
            {
                errors.Add("Purchase date must be today or earlier.");
            }

            if(this.OdometerReading <= currentOdometer)
            {
                errors.Add(string.Format("Odometer must be greater than {0:N0}.", currentOdometer));
            }

            if (this.MilesDriven <= 0)
            {
                errors.Add("Miles driven must be greater than 0.");
            }

            if(this.PricePerUnitPurchased <= 0)
            {
                errors.Add("Price per gallon must be greater than 0.");
            }

            if (this.QuantityUnitsPurchased <=0 ) 
            {
                errors.Add("Gallons must be greater than 0.");
            }

            return errors;
        }
    }
}