﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using PetrolTracker.CodeBehind.Data;
using PetrolTracker.Common;
using PetrolTracker.Common.Infrastructure;
using PetrolTracker.Model;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PetrolTracker.CodeBehind.View 
{
    public partial class AddFillUp : PhoneApplicationPage 
    {
        const string PurchaseDateKey = "PurchaseDateKey";
        const string OdometerKey = "OdometerKey";
        const string GallonsKey = "GallonsKey";
        const string PricePerGallonKey = "PricePerGallonKey";
        const string HasChangesKey = "HasChangesKey";
        bool hasChanges;
        bool isNewInstance;

        public AddFillUp()
        {
            InitializeComponent();
            isNewInstance = true;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // if coming back from dormant state, ignore this block of code
            if(isNewInstance) 
            {
                this.DataContext = DataStore.Fleet.CurrentVehicle;

                // restore view controls if returning from tombstone and values are saved
                if(PhoneApplicationService.Current.StartupMode == StartupMode.Activate && this.State.Count > 0)
                {
                    var pageStateHelper = new PageStateHelper(this.State);
                    this.dpPurchaseDate.Value = pageStateHelper.GetValue<DateTime?>(PurchaseDateKey, DateTime.Now);
                    this.txtOdometer.Text = pageStateHelper.GetValue(OdometerKey, string.Empty);
                    this.txtGallons.Text = pageStateHelper.GetValue(GallonsKey, string.Empty);
                    this.txtPricePerGallon.Text = pageStateHelper.GetValue(PricePerGallonKey, string.Empty);
                    hasChanges = pageStateHelper.GetValue(HasChangesKey, false);
                    CalculateReadOnlyFields();
                }
            }
            isNewInstance = false;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) 
        {
            base.OnNavigatedFrom(e);

            // if not navigating back, save page state.
            if(e.NavigationMode == NavigationMode.Back) 
            {
                return;
            }
            this.State[PurchaseDateKey] = this.dpPurchaseDate.Value;
            this.State[OdometerKey] = this.txtOdometer.Text;
            this.State[GallonsKey] = this.txtGallons.Text;
            this.State[PricePerGallonKey] = this.txtPricePerGallon.Text;
            this.State[HasChangesKey] = hasChanges;
        }

        protected override void OnBackKeyPress(CancelEventArgs e) 
        {
            base.OnBackKeyPress(e);

            // prompt user if they use the back button and have unsaved changes 
            if(!hasChanges) return;
            e.Cancel = (MessageBox.Show("You have unsaved changes, continue?", "Unsaved Data", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel);
        }

        void btnSave_Click(object sender, EventArgs e) 
        {
            var errors = new List<string>();
            int odometerReading;
            double pricePerUnitPurchased;
            double quantityUnitsPurchased;

            // sanity check for nulls, should not happen.
            if(!this.dpPurchaseDate.Value.HasValue) 
            {
                errors.Add("Purchase date is required.");
            }

            // locate any type mismatch errors, example an "a" in a numeric field.
            // note: this is not object validation

            if(!int.TryParse(this.txtOdometer.Text, out odometerReading)) 
            {
                errors.Add("Odometer is not a valid whole number.");
            }

            if(!double.TryParse(this.txtGallons.Text, out quantityUnitsPurchased)) 
            {
                errors.Add("Gallons is not a valid number.");
            }

            if(!double.TryParse(this.txtPricePerGallon.Text, out pricePerUnitPurchased)) 
            {
                errors.Add("Price per gallon is not a valid number.");
            }

            if(errors.Count > 0) 
            {
                MessageBox.Show(string.Join(Environment.NewLine, errors.ToArray()), "Invalid Input", MessageBoxButton.OK);
                return;
            }

            // create and populate a new fill up object
            // Note: the dpPurchaseDate.Value has already been checked for null condition
            var fillUp = new FillUp 
                            {
                                DatePurchased = this.dpPurchaseDate.Value.Value,
                                MilesDriven = odometerReading - DataStore.Fleet.CurrentVehicle.CurrentOdometer,
                                OdometerReading = odometerReading,
                                PricePerUnitPurchased = pricePerUnitPurchased,
                                QuantityUnitsPurchased = quantityUnitsPurchased
                            };

            // validate new fill up object
            // fillUp.Validate returns a collection of strings for any errors.
            errors = new List<string>(fillUp.Validate(DataStore.Fleet.CurrentVehicle.CurrentOdometer));

            if(errors.Count > 0) 
            {
                MessageBox.Show(string.Join(Environment.NewLine, errors.ToArray()), "Invalid Input", MessageBoxButton.OK);
                return;
            }

            // if no errors then add to fill ups collection and persist the data to application storage
            DataStore.Fleet.CurrentVehicle.FillUps.Add(fillUp);
            IsolatedStorageSettings.ApplicationSettings[Constants.FleetKey] = DataStore.Fleet;
            IsolatedStorageSettings.ApplicationSettings.Save();

            this.NavigationService.GoBack();
        }

        void dpPurchaseDate_ValueChanged(object sender, DateTimeValueChangedEventArgs e) 
        {
            hasChanges = true;
        }

        void AllTextBox_TextChanged(object sender, TextChangedEventArgs e) 
        {
            hasChanges = true;
            CalculateReadOnlyFields();
        }

        void CalculateReadOnlyFields() 
        {
            this.tbFillUpMpg.Text = string.Empty;
            this.tbMilesDriven.Text = string.Empty;
            this.tbTotalPurchase.Text = string.Empty;

            int odometerReading;
            int milesDriven = 0;
            double pricePerUnitPurchased;
            double quantityUnitsPurchased;

            int.TryParse(this.txtOdometer.Text, out odometerReading);
            double.TryParse(this.txtGallons.Text, out quantityUnitsPurchased);
            double.TryParse(this.txtPricePerGallon.Text, out pricePerUnitPurchased);

            // the below logic allows for none, some, or all read only values to be displayed.

            if(odometerReading > DataStore.Fleet.CurrentVehicle.CurrentOdometer) 
            {
                milesDriven = odometerReading - DataStore.Fleet.CurrentVehicle.CurrentOdometer;
                this.tbMilesDriven.Text = string.Format("{0:N0}", milesDriven);
            }

            if(milesDriven > 0 && quantityUnitsPurchased > 0) 
            {
                this.tbFillUpMpg.Text = string.Format("{0:N1}", milesDriven / quantityUnitsPurchased);
            }

            if(pricePerUnitPurchased > 0 && quantityUnitsPurchased > 0) 
            {
                this.tbTotalPurchase.Text = string.Format("{0:N2}", pricePerUnitPurchased * quantityUnitsPurchased);
            }
        }
    }
}