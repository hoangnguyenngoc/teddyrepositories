﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Navigation;
using PetrolTracker.CodeBehind.Data;
using PetrolTracker.Common;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PetrolTracker.Common.Infrastructure;

namespace PetrolTracker.CodeBehind.View
{
    public partial class VehicleHistory : PhoneApplicationPage
    {
        const string SelectedChartKey = "SelectedChartKey";
        const string PivotSelectedIndexKey = "PivotSelectedIndexKey";
        bool isNewInstance;

        public VehicleHistory()
        {
            InitializeComponent();
            DataStore.Fleet.CurrentVehicleChanged += (s, e) => SetCurrentVehicle();
            DataStore.Fleet.CurrentVehicleFillUpAdded += (s, e) => RefreshFillUps();
            isNewInstance = true;
            SetCurrentVehicle();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (isNewInstance)
            {
                // this demonstrates an application setting that survives application restarts
                string selectedChart;
                IsolatedStorageSettings.ApplicationSettings.TryGetValue(SelectedChartKey, out selectedChart);
                VisualStateManager.GoToState(this, selectedChart == Constants.CostChartStateName ? Constants.CostChartStateName : Constants.MpgChartStateName, true);

                // this demonstrates a page setting that survives tombstonning
                if (PhoneApplicationService.Current.StartupMode == StartupMode.Activate && this.State.Count > 0)
                {
                    var pageStateHelper = new PageStateHelper(this.State);
                    this.vehicleHistoryPivot.SelectedIndex = pageStateHelper.GetValue(PivotSelectedIndexKey, 0);
                }
            }
            isNewInstance = false;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            IsolatedStorageSettings.ApplicationSettings[SelectedChartKey] = this.mpgHistoryChart.Visibility == Visibility.Visible ? Constants.MpgChartStateName : Constants.CostChartStateName;

            // if not navigating back, save page state.
            if (e.NavigationMode == NavigationMode.Back)
            {
                return;
            }
            this.State[PivotSelectedIndexKey] = this.vehicleHistoryPivot.SelectedIndex;
        }

        void btnSettings_Click(object sender, EventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/View/Settings.xaml", UriKind.Relative));
        }

        void btnAddFillUp_Click(object sender, EventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/View/AddFillUp.xaml", UriKind.Relative));
        }

        void SetCurrentVehicle()
        {
            this.DataContext = DataStore.Fleet.CurrentVehicle;
            RefreshFillUps();
        }

        void RefreshFillUps()
        {
            var cvs = (CollectionViewSource)this.Resources["fillUpsCvs"];
            cvs.Source = DataStore.Fleet.CurrentVehicle.FillUps.Where(f => f.QuantityUnitsPurchased > 0).OrderByDescending(f => f.DatePurchased).ThenByDescending(f => f.OdometerReading).ToList();
        }
    }
}