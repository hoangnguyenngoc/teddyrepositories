﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System;
using System.Windows.Controls;
using PetrolTracker.CodeBehind.Data;
using PetrolTracker.Model;
using Microsoft.Phone.Controls;

namespace PetrolTracker.CodeBehind.View 
{
    public partial class Settings : PhoneApplicationPage 
    {
        public Settings()
        {
            InitializeComponent();
            this.FleetVehicles.ItemsSource = DataStore.Fleet.Vehicles;
            this.FleetVehicles.SelectedItem = DataStore.Fleet.CurrentVehicle;
            this.FleetVehicles.SelectionChanged += FleetVehicles_SelectionChanged;
        }

        void FleetVehicles_SelectionChanged(object sender, SelectionChangedEventArgs e) 
        {
            if (e.AddedItems != null && e.AddedItems[0] != null) 
            {
                DataStore.Fleet.SetCurrentVehicle(((Vehicle) e.AddedItems[0]).VehicleId);
                this.NavigationService.GoBack();
            }
        }
    }
}