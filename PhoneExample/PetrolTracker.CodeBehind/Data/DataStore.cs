﻿//===============================================================================
// Microsoft patterns & practices
// Developing a Windows Phone Application using the MVVM Pattern
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// This code released under the terms of the 
// Microsoft patterns & practices license (http://wp7guide.codeplex.com/license)
//===============================================================================


using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows.Media.Imaging;
using PetrolTracker.Common;
using PetrolTracker.Model;

namespace PetrolTracker.CodeBehind.Data 
{
    /// <summary>
    /// Represents the data store.
    /// </summary>
    public static class DataStore 
    {
        static Fleet fleet;

        /// <summary>
        /// Gets the fleet.
        /// </summary>
        public static Fleet Fleet 
        {
            get
            {
                if (fleet != null) 
                {
                    return fleet;
                }
                fleet = (Fleet) IsolatedStorageSettings.ApplicationSettings[Constants.FleetKey];

                //this provides fleet initialization after being restored from isolated storage
                fleet.SetCurrentVehicle(fleet.CurrentVehicleId);
                LoadVehicleImages();
                return fleet;
            }
        }

        /// <summary>
        /// Loads the vehicle image files from isolated storage into memory 
        /// </summary>
        static void LoadVehicleImages() 
        {
            using (var isf = IsolatedStorageFile.GetUserStoreForApplication()) 
            {
                foreach (var vehicle in Fleet.Vehicles.Where(vehicle => isf.FileExists(vehicle.PictureFileName))) 
                {
                    vehicle.Picture = new BitmapImage();
                    using (IsolatedStorageFileStream fs = isf.OpenFile(vehicle.PictureFileName, FileMode.Open, FileAccess.Read)) 
                    {
                        vehicle.Picture.SetSource(fs);
                    }
                }
            }
        }
    }
}